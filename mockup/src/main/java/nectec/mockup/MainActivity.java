package nectec.mockup;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static android.view.View.GONE;

public class MainActivity extends AppCompatActivity {

  private EditText score;
  private EditText group;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    group = (EditText) findViewById(R.id.group);
    score = (EditText) findViewById(R.id.score);

    String action = getIntent().getAction();
    switch (action) {
      case "th.in.ffc.intent.action.TAI":
        score.setVisibility(GONE);
        break;
      case "th.in.ffc.intent.action.ADL":
        group.setVisibility(GONE);
        break;
    }

    findViewById(R.id.send).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (TextUtils.isEmpty(group.getText()) && TextUtils.isEmpty(score.getText())) {
          Toast.makeText(MainActivity.this, "Please fill input", Toast.LENGTH_SHORT).show();
          return;
        }

        Intent data = new Intent();
        data.putExtra("data.group", group.getText().toString());
        try {
          data.putExtra("data.score", Integer.parseInt(score.getText().toString()));
        } catch (NumberFormatException ignore) {
        }
        setResult(RESULT_OK, data);
        finish();
      }
    });
    TextView header = (TextView) findViewById(R.id.header);
    header.setText(getIntent().getAction());
  }


}
