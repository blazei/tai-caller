package nectec.tai.caller

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.alert


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tai.setOnClickListener { safeStartActivityForResult(Intent(TAI_ACTION), 1) }
        adl.setOnClickListener { safeStartActivityForResult(Intent(ADL_ACTION), 2) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (resultCode) {
            Activity.RESULT_OK -> onResultOk(requestCode, data)
            Activity.RESULT_CANCELED -> onResultCanceled(requestCode, data)
            else -> alert("Not support resultCode!!").show()
        }
    }

    private fun onResultOk(requestCode: Int, data: Intent?) {
        header.text = actionFromRequest(requestCode)
        when (requestCode) {
            1 -> {
                result.text = data?.inspectResult(TAI_ACTION)
                rawData.text = "กลุ่ม ${data?.group}"
            }
            2 -> {
                result.text = data?.inspectResult(ADL_ACTION)
                rawData.text = "คะแนน ${data?.score}"
            }
        }
    }

    private fun onResultCanceled(requestCode: Int, data: Intent?) {
        alert("Request for ${actionFromRequest(requestCode)} was canceled").show()
        header.text = null
        result.text = null
        rawData.text = null
    }

    private fun actionFromRequest(requestCode: Int): String = when (requestCode) {
        1 -> "TAI"
        2 -> "ADL"
        else -> "Unknown"
    }

    private fun Activity.safeStartActivityForResult(intent: Intent, requestCode: Int = -1) {
        try {
            startActivityForResult(intent, requestCode)
        } catch (activityNotFound: ActivityNotFoundException) {
            alert("Not found support activity").show()
        }
    }

    private val TAI_ACTION: String
        get() = "th.in.ffc.intent.action.TAI"
    private val ADL_ACTION: String
        get() = "th.in.ffc.intent.action.ADL"

    private val EXTRA_SCORE
        get() = "data.score"
    private val EXTRA_GROUP
        get() = "data.group"

    private val Intent.score: Int
        get() = getIntExtra(EXTRA_SCORE, -1)
    private val Intent.group: String?
        get() = getStringExtra(EXTRA_GROUP)

    private fun Intent.inspectResult(action: String): String = when (action) {
        TAI_ACTION -> {
            when (group?.toUpperCase()) {
                "B5", "B4", "B3" -> "ติดสังคม"
                "C4", "C3", "C2" -> "ติดบ้าน"
                "I3" -> "ติดเตียง 1"
                "I2", "I1" -> "ติดเตียง 2"
                null -> "ไม่ระบุ!"
                else -> "ERR!"
            }
        }
        ADL_ACTION -> {
            when {
                score > 11 -> "ติดสังคม"
                score >= 5 -> "ติดบ้าน"
                score >= 0 -> "ติดเตียง"
                else -> "ERR!"
            }
        }
        else -> "What?"
    }
}
